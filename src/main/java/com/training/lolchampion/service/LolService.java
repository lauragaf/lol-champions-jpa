package com.training.lolchampion.service;

import com.training.lolchampion.entities.LolChampion;
import com.training.lolchampion.repository.LolChampionshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolService {

    @Autowired
    LolChampionshipRepository lolChampionshipRepository;

    public List<LolChampion> findAll(){
        return this.lolChampionshipRepository.findAll();
    }


    public LolChampion save(LolChampion lolChampion) {
        return lolChampionshipRepository.save(lolChampion);
    }
}